# WebDev #
This repository contains a Vagrantfile and a Puppet manifest used to set up an environment suitable for developing Greene King websites.

The following tasks are completed during provisioning:

* Install Nginx (via EPEL), MySQL server and PHP-FPM
* Install Git, npm, Node.js and various PHP packages
* Install the less and autoprefixer packages via npm
* Install drush via pear
* Create /etc/drush and place drushrc PHP scripts there
* Create drush user with SSH keys
* Copy SSH keys into root user home directory (if source files exist)
* Put useful shell scripts in /usr/local/bin
    - git-subrepo-list
    - git-subrepo-status
    - puppet-update
    - drush-site-sync
    - drush-fd-overridden
* Put a Drupal specific Nginx configuration file in /etc/nginx with an example virtual host file showing how to make use of it
* Start the Nginx, PHP-FPM and MySQL services
* Turn the firewall off so that we don't have to punch a hole in it just to get a local development environment working
* Create hosts file entries for Rackspace servers

## Requirements ##
* [VirtualBox ~4.3.20](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant ~1.7.2](https://www.vagrantup.com/downloads.html)
* NFS: `apt-get install nfs-kernel-server`

## Getting started ##
Execute the following commands to get the development environment up and running:
```
mkdir ~/webdev && cd $_
git clone git@bitbucket.org:greeneking/webdev.git .
cd vagrant
vagrant up
```
The last command will take a while (depending on the speed of your internet connection) because the VirtualBox disk image specified by the Vagrantfile needs to be downloaded to your machine (this will only happen the first time `vagrant up` is run for any given box because thereafter it will be retrieved from the local machine).

## Post boot tasks ##

* Add `max_allowed_packet=500M` to file `/etc/my.cnf` in the `[mysqld]` section. This ensures that the MySQL server won't throw an error when performing large queries (e.g. when syncing websites)
* Add `/usr/local/bin` to the `PATH` variable in file `/root/.bash_profile`. This makes it possible to run useful shell scripts without having to type the full path (e.g. `drush-fd-overridden`)
* Turn off Nginx sendfile feature in file `/etc/nginx/nginx.conf`. This prevents Nginx from aggressively caching certain types of files in some circumstances
* Set Git user name/email configuration:
    - `git config --global user.name "Your Name"`
    - `git config --global user.email you@example.com`

### Implementing git completion/prompt ###
It is useful to have Git auto-complete commands for you, and also show the state of the repository you are in the folder of as part of the command prompt. The following process should achieve these:
```
sudo -i
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
```
Edit file `/root/.bash_profile` and add the following lines:
```
. ~/git-completion.bash
. ~/git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=1
export PS1='\w$(__git_ps1 " (%s)")\$ '
```