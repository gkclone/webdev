node default {
  #
  # Define defaults for packages and services.
  #
  Package {
    ensure => 'installed',
    allow_virtual => false
  }

  Service {
    ensure => 'running',
    enable => true
  }

  #
  # Add EPEL repository and install Nginx.
  #
  yumrepo { 'epel':
    baseurl => 'http://dl.fedoraproject.org/pub/epel/6/x86_64',
    descr => 'Extra Packages for Enterprise Linux',
    enabled => 1,
    gpgcheck => 0
  }

  package { 'nginx': require => Yumrepo['epel'] }

  #
  # Install other important/useful packages.
  #
  $packages = [
    'git',
    'mysql-server',
    'nodejs',
    'npm',
    'php-fpm',
    'php-gd',
    'php-mbstring',
    'php-mcrypt',
    'php-mysql',
    'php-pdo',
    'php-pear',
    'php-xml'
  ]
  package { $packages: }

  #
  # Install the less and autoprefixer packages via npm.
  #
  exec { 'npm install less':
    path => '/bin:/usr/bin',
    command => 'npm install -g less',
    unless => 'npm list -g --depth=0 | grep less@',
    require => Package['npm']
  }

  exec { 'npm install autoprefixer':
    path => '/bin:/usr/bin',
    command => 'npm install -g autoprefixer',
    unless => 'npm list -g --depth=0 | grep autoprefixer@',
    require => Package['npm']
  }

  #
  # Install drush via pear.
  #
  exec { 'pear channel-discover drush':
    path => '/bin:/usr/bin',
    command => 'pear channel-discover pear.drush.org',
    unless => 'pear list-channels | grep pear.drush.org',
    require => Package['php-pear']
  }

  exec { 'pear install drush':
    path => '/bin:/usr/bin',
    command => 'pear install drush/drush',
    onlyif => 'pear list -c pear.drush.org | grep "no packages installed"',
    require => Exec['pear channel-discover drush']
  }

  #
  # Create /etc/drush and place drushrc PHP scripts there.
  #
  file { '/etc/drush': ensure => 'directory' }

  file { '/etc/drush/aliases.drushrc.php':
    source => '/vagrant/puppet/files/drush/aliases/aliases.drushrc.php',
    owner => 'root',
    group => 'root',
    require => File['/etc/drush']
  }

  file { '/etc/drush/example.projects.aliases.drushrc.php':
    source => '/vagrant/puppet/files/drush/aliases/example.projects.aliases.drushrc.php',
    owner => 'root',
    group => 'root',
    require => File['/etc/drush']
  }

  file { '/etc/drush/example.settings.aliases.drushrc.php':
    source => '/vagrant/puppet/files/drush/aliases/example.settings.aliases.drushrc.php',
    owner => 'root',
    group => 'root',
    require => File['/etc/drush']
  }

  #
  # Create drush user with SSH keys.
  #
  user { 'drush':
    ensure => 'present',
    comment => 'Drush',
    home => '/home/drush',
    managehome => true
  }

  file { '/home/drush/.ssh':
    ensure =>  'directory',
    owner =>  'drush',
    group =>  'drush',
    mode =>  '0700'
  }

  file { '/home/drush/.ssh/id_rsa':
    source => '/vagrant/puppet/files/drush/.ssh/id_rsa',
    mode => '0600'
  }

  file { '/home/drush/.ssh/id_rsa.pub':
    source => '/vagrant/puppet/files/drush/.ssh/id_rsa.pub',
    mode => '0644'
  }

  #
  # Copy SSH keys into root user home directory (if source files exist).
  #
  file { '/root/.ssh': ensure => 'directory' }

  $id_rsa = file('/vagrant/puppet/files/root/.ssh/id_rsa', '/dev/null')
  $id_rsa_pub = file('/vagrant/puppet/files/root/.ssh/id_rsa.pub', '/dev/null')

  if ($id_rsa != '') {
    file { '/root/.ssh/id_rsa':
      content => $id_rsa,
      mode => '0600',
      require => File['/root/.ssh']
    }
  }

  if ($id_rsa_pub != '') {
    file { '/root/.ssh/id_rsa.pub':
      content => $id_rsa_pub,
      mode => '0600',
      require => File['/root/.ssh']
    }
  }

  #
  # Put useful shell scripts in /usr/local/bin.
  #
  file { '/usr/local/bin/git-subrepo-list':
    source => '/vagrant/puppet/files/git/git-subrepo-list',
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/usr/local/bin/git-subrepo-status':
    source => '/vagrant/puppet/files/git/git-subrepo-status',
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/usr/local/bin/puppet-update':
    source => '/vagrant/puppet/files/puppet/puppet-update',
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/usr/local/bin/drush-site-sync':
    source => '/vagrant/puppet/files/drush/drush-site-sync',
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  file { '/usr/local/bin/drush-fd-overridden':
    source => '/vagrant/puppet/files/drush/drush-fd-overridden',
    owner => 'root',
    group => 'root',
    mode => '0755'
  }

  #
  # Put a Drupal specific Nginx configuration file in /etc/nginx with an example
  # virtual host file showing how to make use of it.
  #
  file { '/etc/nginx/drupal.conf':
    source => '/vagrant/puppet/files/nginx/drupal.conf',
    owner => 'root',
    group => 'root',
    mode => '0644',
    require => Package['nginx']
  }

  file { '/etc/nginx/vhost-example.conf':
    source => '/vagrant/puppet/files/nginx/vhost-example.conf',
    owner => 'root',
    group => 'root',
    mode => '0644',
    require => Package['nginx']
  }

  #
  # Start services.
  #
  service { 'nginx': require => Package['nginx'] }
  service { 'php-fpm': require => Package['php-fpm'] }
  service { 'mysqld': require => Package['mysql-server'] }

  # Turn the firewall off so that we don't have to punch a hole in it just to
  # get a local development environment working.
  service { 'iptables': ensure => 'stopped' }

  #
  # Create hosts file entries for Rackspace servers.
  #
  host { 'pp-web': ip => '162.13.37.248' }
  host { 'pp-db': ip => '162.13.37.249' }
  host { 'hh-db': ip => '162.13.48.16' }
  host { 'ei': ip => '162.13.48.17' }
  host { 'lfsg-web': ip => '162.13.48.18' }
  host { 'lfsg-db': ip => '162.13.48.19' }
  host { 'locals-web': ip => '162.13.48.20' }
  host { 'locals-db': ip => '162.13.48.21' }
  host { 'belhaven': ip => '162.13.48.23' }
  host { 'stage': ip => '162.13.48.24' }
  host { 'oei-web': ip => '162.13.48.26' }
  host { 'bb': ip => '162.13.48.27' }
  host { 'oei-db': ip => '162.13.48.28' }
  host { 'hh-web': ip => '162.13.48.30' }
}
