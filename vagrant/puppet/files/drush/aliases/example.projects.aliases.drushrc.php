<?php

/**
 * Projects defined here will be used to create a suite of drush site aliases.
 * @see aliases.drushrc.php
 */

$projects = array(
  'projectcode' => array(
    'live' => array(
      'uri' => 'www.example.com',
      'host' => '11.22.33.44',
    ),
    'development' => array(
      'branches' => array(
        'feature-example',
      ),
    ),
  ),
);

?>
